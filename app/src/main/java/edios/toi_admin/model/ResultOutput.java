package edios.toi_admin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultOutput implements Parcelable {

    public static final Creator<ResultOutput> CREATOR = new Creator<ResultOutput>() {
        @Override
        public ResultOutput createFromParcel(Parcel in) {
            return new ResultOutput(in);
        }

        @Override
        public ResultOutput[] newArray(int size) {
            return new ResultOutput[size];
        }

    };
    @SerializedName("customerOrders")
    private List<CustomerOrdersItem> customerOrders;

    protected ResultOutput(Parcel in) {
        this.customerOrders = in.createTypedArrayList(CustomerOrdersItem.CREATOR);
    }

    public ResultOutput() {

    }

    public List<CustomerOrdersItem> getCustomerOrders() {
        return customerOrders;
    }


    @Override
    public String toString() {
        return "ResultOutput{" + "customerOrders = '" + customerOrders + '\'' + "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.customerOrders);
    }
}