package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;

public class UpdateOrderRequest {
    @SerializedName("signatureKey")
    private String signatureKey;
    @SerializedName("userName")
    private String userName;
    @SerializedName("appEnd")
    private String appEnd;
    @SerializedName("orderToBeUpdated")
    private CustomerOrdersItem orderToBeUpdated;

    public String getSignatureKey() {
        return signatureKey;
    }

    public void setSignatureKey(String signatureKey) {
        this.signatureKey = signatureKey;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setAppEnd(String appEnd) {
        this.appEnd = appEnd;
    }

    public void setOrderToBeUpdated(CustomerOrdersItem orderToBeUpdated) {
        this.orderToBeUpdated = orderToBeUpdated;
    }
}
