package edios.toi_admin.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edios.toi_admin.R;
import edios.toi_admin.interfaces.NotifyFetchOrder;
import edios.toi_admin.model.CustomerOrdersItem;
import edios.toi_admin.model.FetchOrderResponse;
import edios.toi_admin.model.ResultOutput;
import edios.toi_admin.model.UpdateOrderRequest;
import edios.toi_admin.network.NetworkSingleton;
import edios.toi_admin.ui.activities.OrderInfoActivity;
import edios.toi_admin.utils.AppConstants;
import edios.toi_admin.utils.SharedPref;
import edios.toi_admin.utils.Utils;
import edios.toi_admin.utils.printing.BluetoothService;
import edios.toi_admin.utils.printing.SingletonBluetoothService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersRecyclerAdapter extends RecyclerView.Adapter<OrdersRecyclerAdapter.ViewHolder> {

    String[] print_size_array;
    private Context context;
    private List<CustomerOrdersItem> customerOrders;
    private Utils utils;
    private boolean showUpdateStatus;

    public OrdersRecyclerAdapter(Context context, List<CustomerOrdersItem> customerOrders, boolean showUpdateStatus) {
        this.context = context;
        this.customerOrders = customerOrders;
        this.utils = new Utils(context);
        this.showUpdateStatus = showUpdateStatus;
        print_size_array = context.getResources().getStringArray(R.array.print_size_array);

    }

    public void updateList(List<CustomerOrdersItem> customerOrders) {
        this.customerOrders = customerOrders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_recycler_item, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CustomerOrdersItem order = customerOrders.get(position);
        holder.tv_orderNumber.setText(order.getOrderNumber() + " (" + order.getOrderStatus() + ")");
        holder.tv_orderStatus.setText(order.getOrderDateTime());
        holder.rv_itemRecyclerView.setAdapter(new OrderItemsRecyclerAdapter(context, order.getPreviousOrderItems(), false));
        holder.tv_customerName.setText(order.getCustomer().getUserName());
        holder.tv_customerMobile.setText(order.getCustomer().getMobileNo());
        System.out.println(order.getTotalDiscountAmount() + " discount ba");
        System.out.println(order.getTotalTaxAmount() + " tax ba");
        if (order.getTotalDiscountAmount() != 0) {
            holder.tv_totalDiscount.setVisibility(View.VISIBLE);
            holder.tv_totalDiscount.setText("Total Discount: $" + Utils.decimalFormat.format(order.getTotalDiscountAmount()));
        } else {
            holder.tv_totalDiscount.setVisibility(View.GONE);
        }
        if (order.getTotalTaxAmount() != 0) {
            holder.tv_totalTax.setVisibility(View.VISIBLE);
            holder.tv_totalTax.setText("Total Tax: $" + Utils.decimalFormat.format(order.getTotalTaxAmount()));
        } else {
            holder.tv_totalTax.setVisibility(View.GONE);
        }
        holder.tv_orderTotalAmount.setText("Total Amount: $" + Utils.decimalFormat.format(order.getTotalOrderAmount()));
    }

    private UpdateOrderRequest preapareUpdateOrderRequest(CustomerOrdersItem order, String status) {
        UpdateOrderRequest updateOrderRequest = new UpdateOrderRequest();
        updateOrderRequest.setAppEnd(AppConstants.APP_END);
        order.setOrderStatus(status);
        updateOrderRequest.setOrderToBeUpdated(order);
        updateOrderRequest.setSignatureKey(AppConstants.SIGNATURE_KEY);
        updateOrderRequest.setUserName(SharedPref.getReader(context).getString(AppConstants.USER_NAME, "ADMIN"));

        return updateOrderRequest;
    }

    @Override
    public int getItemCount() {
        return customerOrders != null ? customerOrders.size() : 0;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_orderNumber)
        TextView tv_orderNumber;
        @BindView(R.id.tv_orderStatus)
        TextView tv_orderStatus;
        @BindView(R.id.tv_customerName)
        TextView tv_customerName;
        @BindView(R.id.tv_customerMobile)
        TextView tv_customerMobile;
        @BindView(R.id.tv_orderTotalAmount)
        TextView tv_orderTotalAmount;
        @BindView(R.id.tv_totalDiscount)
        TextView tv_totalDiscount;
        @BindView(R.id.tv_totalTax)
        TextView tv_totalTax;
        @BindView(R.id.rv_items)
        RecyclerView rv_itemRecyclerView;
        @BindView(R.id.btn_updateStatus)
        Button btn_updateStatus;
        @BindView(R.id.btn_printOrder)
        Button btn_printOrder;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (!showUpdateStatus) {
                btn_updateStatus.setVisibility(View.GONE);
                btn_printOrder.setVisibility(View.GONE);
            }

        }

        @OnClick({R.id.btn_updateStatus, R.id.btn_printBill, R.id.btn_printOrder, R.id.iv_orderInfo})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_updateStatus:
                    int index = Arrays.asList(context.getResources().getStringArray(R.array.status_array)).
                            indexOf(customerOrders.get(getAdapterPosition()).getOrderStatus());
                    utils.updateStatusDialog(index, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                            updateOrder(text.toString());
                            return false;
                        }
                    });
                    break;
                case R.id.iv_orderInfo:
                    Intent orderInfoIntent = new Intent(context, OrderInfoActivity.class);
                    orderInfoIntent.putExtra("orderInfo", customerOrders.get(getAdapterPosition()));
                    context.startActivity(orderInfoIntent);
                    break;
                case R.id.btn_printBill:
                    if (SingletonBluetoothService.getInstance(context, null).mState == BluetoothService.STATE_CONNECTED) {
                        String selectedPrintSize = SharedPref.getReader(context).getString(AppConstants.PRINT_SIZE, print_size_array[0]);
                        if (selectedPrintSize.equalsIgnoreCase(print_size_array[0]))
                            utils.print3InchTSPBill(customerOrders.get(getAdapterPosition()));
                        else if (selectedPrintSize.equalsIgnoreCase(print_size_array[1]))
                            utils.print3InchBill(customerOrders.get(getAdapterPosition()));
                        else utils.print2InchBill(customerOrders.get(getAdapterPosition()));
                    } else {
                        Toast.makeText(context, AppConstants.BLUETOOTH_UNAVAILABLE_MESSAGE, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.btn_printOrder:
                    if (SingletonBluetoothService.getInstance(context, null).mState == BluetoothService.STATE_CONNECTED) {
                        String selectedPrintSize = SharedPref.getReader(context).getString(AppConstants.PRINT_SIZE, print_size_array[0]);
                        if (selectedPrintSize.equalsIgnoreCase(print_size_array[0]))
                            utils.print3InchTSPKitchenTicket(customerOrders.get(getAdapterPosition()));
                        else if (selectedPrintSize.equalsIgnoreCase(print_size_array[1]))
                            utils.print3InchKitchenTicket(customerOrders.get(getAdapterPosition()));
                        else
                            utils.print2InchKitchenTicket(customerOrders.get(getAdapterPosition()));

                    } else {
                        Toast.makeText(context, AppConstants.BLUETOOTH_UNAVAILABLE_MESSAGE, Toast.LENGTH_SHORT).show();
                    }
                    break;


            }

        }


        void updateOrder(String orderStatus) {
            final MaterialDialog dialog = utils.showProgressDialog("Updating Order ...");
            NetworkSingleton.getInstance(context).updateOrder(preapareUpdateOrderRequest(customerOrders.get(getAdapterPosition()), orderStatus)).enqueue(new Callback<FetchOrderResponse>() {
                @Override
                public void onResponse(@NonNull Call<FetchOrderResponse> call, @NonNull Response<FetchOrderResponse> response) {
                    dialog.dismiss();
                    if(response.body()!=null) {
                        new Utils(context).fetchOrders(new NotifyFetchOrder() {
                            @Override
                            public void orderFetched(ResultOutput orders, boolean isSuccess) {
                                context.sendBroadcast(new Intent(AppConstants.UPDATE_ORDERS).putExtra(AppConstants.UPDATE_ORDERS, orders));
                            }

                        }, "");
                    }else{
                        Toast.makeText(context, AppConstants.SERVER_NOT_RESPONDING_MESSAGE, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<FetchOrderResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(context, AppConstants.SERVER_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


}
