package edios.toi_admin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerOrdersItem implements Parcelable {
    public static final Creator<CustomerOrdersItem> CREATOR = new Creator<CustomerOrdersItem>() {
        @Override
        public CustomerOrdersItem createFromParcel(Parcel in) {
            return new CustomerOrdersItem(in);
        }

        @Override
        public CustomerOrdersItem[] newArray(int size) {
            return new CustomerOrdersItem[size];
        }
    };
    @SerializedName("orderDateTime")
    private String orderDateTime;
    @SerializedName("orderType")
    private String orderType;
    @SerializedName("orderNumber")
    private String orderNumber;
    @SerializedName("previousOrderItems")
    private List<PreviousOrderItems> previousOrderItems;
    @SerializedName("totalPrice")
    private double totalPrice;
    @SerializedName("totalDiscountAmount")
    private double totalDiscountAmount;
    @SerializedName("customerOrderID")
    private int customerOrderID;
    @SerializedName("orderStatus")
    private String orderStatus;
    @SerializedName("paymentType")
    private String paymentType;
    @SerializedName("totalOrderAmount")
    private double totalOrderAmount;
    @SerializedName("orderStatusDateTime")
    private String orderStatusDateTime;
    @SerializedName("customerUserID")
    private int customerUserID;
    @SerializedName("totalTaxAmount")
    private double totalTaxAmount;
    @SerializedName("paymentStatus")
    private String paymentStatus;
    @SerializedName("orderReadyTimeInmin")
    private int orderReadyTimeInmin;
    @SerializedName("customer")
    private Customer customer;
    @SerializedName("orderInstructions")
    private String orderInstructions;
    @SerializedName("orderRating")
    private int orderRating;
    @SerializedName("orderReview")
    private String orderReview;
    @SerializedName("orderPickupDateTime")
    private String orderPickupDateTime;


    protected CustomerOrdersItem(Parcel in) {
        orderDateTime = in.readString();
        orderPickupDateTime = in.readString();
        orderType = in.readString();
        orderNumber = in.readString();
        previousOrderItems = in.createTypedArrayList(PreviousOrderItems.CREATOR);
        totalPrice = in.readDouble();
        totalDiscountAmount = in.readDouble();
        customerOrderID = in.readInt();
        orderStatus = in.readString();
        paymentType = in.readString();
        totalOrderAmount = in.readDouble();
        orderStatusDateTime = in.readString();
        customerUserID = in.readInt();
        totalTaxAmount = in.readDouble();
        paymentStatus = in.readString();
        orderReadyTimeInmin = in.readInt();
        orderInstructions = in.readString();
        orderRating = in.readInt();
        orderReview = in.readString();
        customer = in.readParcelable(Customer.class.getClassLoader());
    }

    public String getOrderPickupDateTime() {
        return orderPickupDateTime;
    }

    public void setOrderPickupDateTime(String orderPickupDateTime) {
        this.orderPickupDateTime = orderPickupDateTime;
    }

    public String getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(String orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<PreviousOrderItems> getPreviousOrderItems() {
        return previousOrderItems;
    }

    public void setPreviousOrderItems(List<PreviousOrderItems> previousOrderItems) {
        this.previousOrderItems = previousOrderItems;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(double totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    public int getCustomerOrderID() {
        return customerOrderID;
    }

    public void setCustomerOrderID(int customerOrderID) {
        this.customerOrderID = customerOrderID;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public double getTotalOrderAmount() {
        return totalOrderAmount;
    }

    public void setTotalOrderAmount(double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public String getOrderStatusDateTime() {
        return orderStatusDateTime;
    }

    public void setOrderStatusDateTime(String orderStatusDateTime) {
        this.orderStatusDateTime = orderStatusDateTime;
    }

    public int getCustomerUserID() {
        return customerUserID;
    }

    public void setCustomerUserID(int customerUserID) {
        this.customerUserID = customerUserID;
    }

    public double getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(int totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public int getOrderReadyTimeInmin() {
        return orderReadyTimeInmin;
    }

    public void setOrderReadyTimeInmin(int orderReadyTimeInmin) {
        this.orderReadyTimeInmin = orderReadyTimeInmin;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getOrderInstructions() {
        return orderInstructions;
    }

    public void setOrderInstructions(String orderInstructions) {
        this.orderInstructions = orderInstructions;
    }

    public int getOrderRating() {
        return orderRating;
    }

    public void setOrderRating(int orderRating) {
        this.orderRating = orderRating;
    }

    public String getOrderReview() {
        return orderReview;
    }

    public void setOrderReview(String orderReview) {
        this.orderReview = orderReview;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(orderDateTime);
        parcel.writeString(orderPickupDateTime);
        parcel.writeString(orderType);
        parcel.writeString(orderNumber);
        parcel.writeTypedList(previousOrderItems);
        parcel.writeDouble(totalPrice);
        parcel.writeDouble(totalDiscountAmount);
        parcel.writeInt(customerOrderID);
        parcel.writeString(orderStatus);
        parcel.writeString(paymentType);
        parcel.writeDouble(totalOrderAmount);
        parcel.writeString(orderStatusDateTime);
        parcel.writeInt(customerUserID);
        parcel.writeDouble(totalTaxAmount);
        parcel.writeString(paymentStatus);
        parcel.writeInt(orderReadyTimeInmin);
        parcel.writeString(orderInstructions);
        parcel.writeInt(orderRating);
        parcel.writeString(orderReview);
        parcel.writeParcelable(customer, i);
    }

    @Override
    public String toString() {
        return "CustomerOrdersItem{" + "orderDateTime='" + orderDateTime + '\'' + ", orderType='" + orderType + '\'' + ", orderNumber='" + orderNumber + '\'' + ", previousOrderItems=" + previousOrderItems + ", totalPrice=" + totalPrice + ", totalDiscountAmount=" + totalDiscountAmount + ", customerOrderID=" + customerOrderID + ", orderStatus='" + orderStatus + '\'' + ", paymentType='" + paymentType + '\'' + ", totalOrderAmount=" + totalOrderAmount + ", orderStatusDateTime='" + orderStatusDateTime + '\'' + ", customerUserID=" + customerUserID + ", totalTaxAmount=" + totalTaxAmount + ", paymentStatus='" + paymentStatus + '\'' + ", orderReadyTimeInmin=" + orderReadyTimeInmin + ", customer=" + customer + ", orderInstructions='" + orderInstructions + '\'' + ", orderRating=" + orderRating + ", orderReview='" + orderReview + '\'' + ", orderPickupDateTime='" + orderPickupDateTime + '\'' + '}';
    }
}