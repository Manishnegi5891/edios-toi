package edios.toi_admin.network;

import edios.toi_admin.model.ChangePasswordRequest;
import edios.toi_admin.model.FetchOrderRequest;
import edios.toi_admin.model.FetchOrderResponse;
import edios.toi_admin.model.GenericResponse;
import edios.toi_admin.model.LoginRequest;
import edios.toi_admin.model.OrderSummaryRequest;
import edios.toi_admin.model.OrderSummaryResponse;
import edios.toi_admin.model.SearchOrderRequest;
import edios.toi_admin.model.SettingsRequest;
import edios.toi_admin.model.UpdateOrderRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Calls {
    @POST("adminLogin")
    Call<GenericResponse> authenticate(@Body LoginRequest loginBean );
    @POST("adminOrders")
    Call<FetchOrderResponse> fetchOrders(@Body FetchOrderRequest fetchOrderRequest);
    @POST("updateOrder")
    Call<FetchOrderResponse> updateOrder(@Body UpdateOrderRequest updateOrderRequest);
    @POST("orderSummary")
    Call<OrderSummaryResponse> orderSummary(@Body OrderSummaryRequest updateOrderRequest);
    @POST("changePassword")
    Call<GenericResponse> changePassword(@Body ChangePasswordRequest changePasswordRequest);
    @POST("filterOrders")
    Call<FetchOrderResponse> searchOrders(@Body SearchOrderRequest searchOrderRequest);
    @POST("adminSettings")
    Call<GenericResponse> updateSettings(@Body SettingsRequest searchOrderRequest);


}