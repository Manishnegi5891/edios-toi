package edios.toi_admin.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import edios.toi_admin.R;
import edios.toi_admin.interfaces.NotifyFetchOrder;
import edios.toi_admin.model.ResultOutput;
import edios.toi_admin.ui.adapter.OrdersRecyclerAdapter;
import edios.toi_admin.utils.StartDatePicker;
import edios.toi_admin.utils.Utils;

public class OrderHistoryFragment extends Fragment implements NotifyFetchOrder, View.OnClickListener, TextWatcher {
    @BindView(R.id.ordersRecycler)
    RecyclerView recyclerView;
    @BindView(R.id.et_date)
    TextView et_date;
    private Utils utils;
    View rootView;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_order_history, null);
        utils = new Utils(getContext());
        unbinder = ButterKnife.bind(this, rootView);

        et_date.addTextChangedListener(this);
        et_date.setText(getYesterdayDate());
        return rootView;
    }

    @Override
    public void orderFetched(ResultOutput orders, boolean isSuccess) {
        recyclerView.setAdapter(new OrdersRecyclerAdapter(getActivity(), orders.getCustomerOrders(), false));
        if (recyclerView.getAdapter().getItemCount() < 1) {
            rootView.findViewById(R.id.tv_noOrderFound).setVisibility(View.VISIBLE);
        }else{
            rootView.findViewById(R.id.tv_noOrderFound).setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.iv_datePicker)
    public void onClick(View view) {
        if (view.getId() == R.id.iv_datePicker) {
            new StartDatePicker(getActivity(), et_date).show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "start_date_picker");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);
        try {
            Date d = sdf1.parse(et_date.getText().toString());
            System.out.println("d = " + d);
            System.out.println(sdf2.format(d));
            utils.fetchOrders(this, sdf2.format(d));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void afterTextChanged(Editable editable) {

    }

    String getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date d = cal.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);
        return sdf1.format(d);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
