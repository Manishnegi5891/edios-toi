package edios.toi_admin.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edios.toi_admin.R;
import edios.toi_admin.interfaces.NotifyFetchOrder;
import edios.toi_admin.model.ResultOutput;
import edios.toi_admin.service.FetchOrderService;
import edios.toi_admin.utils.AppConstants;
import edios.toi_admin.utils.Utils;


public class OrdersFragment extends Fragment implements NotifyFetchOrder {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Utils utils;
    private Intent intent;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.refresh).setVisible(true);// making refresh button visible for order refresh call
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.refresh) utils.fetchOrders(this, "");
        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, null);
        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewpager);
        utils = new Utils(getActivity());
        intent = new Intent(getActivity(), FetchOrderService.class);
        setupViewPager();
        setHasOptionsMenu(true);

        viewPager.setOffscreenPageLimit(4);

        utils.fetchOrders(this, "");

        return view;
    }


    private void setupViewPager() {
        System.out.println("OrdersFragment.setupViewPager");
        if (viewPager != null) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
            adapter.addFragment(new PendingOrderFragment(), getString(R.string.pending_tab));
            adapter.addFragment(new InKitchenOrderFragment(), getString(R.string.in_kitchen_tab));
            adapter.addFragment(new ReadyForPickupFragment(), getString(R.string.ready_for_pickup_tab));
            adapter.addFragment(new CompletedOrderFragment(), getString(R.string.completed_tab));
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
            TextView tv1 = (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(0)).getChildAt(1));
            tv1.setScaleY(-1);
            TextView tv2 = (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(1)).getChildAt(1));
            tv2.setScaleY(-1);
            TextView tv3 = (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(2)).getChildAt(1));
            tv3.setScaleY(-1);
            TextView tv4 = (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(3)).getChildAt(1));
            tv4.setScaleY(-1);
        }

    }

    @Override
    public void orderFetched(ResultOutput orders, boolean isSuccess) {
        if (isSuccess) {
            getActivity().sendBroadcast(new Intent(AppConstants.UPDATE_ORDERS).putExtra(AppConstants.UPDATE_ORDERS, orders));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().startService(intent);

    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().stopService(intent);
        System.out.println("OrdersFragment.onStop");
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
