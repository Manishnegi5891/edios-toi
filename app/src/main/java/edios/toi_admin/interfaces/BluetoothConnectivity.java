package edios.toi_admin.interfaces;

public interface BluetoothConnectivity {
    void isConnected(boolean isConnect);
}
