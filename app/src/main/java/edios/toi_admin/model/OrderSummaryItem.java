package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;

public class OrderSummaryItem{

	@SerializedName("totalAmount")
	private double totalAmount;

	@SerializedName("totalOrders")
	private int totalOrders;

	@SerializedName("orderDate")
	private String orderDate;

	public double getTotalAmount(){
		return totalAmount;
	}

	public int getTotalOrders(){
		return totalOrders;
	}

	public String getOrderDate(){
		return orderDate;
	}

	@Override
 	public String toString(){
		return 
			"OrderSummaryItem{" + 
			"totalAmount = '" + totalAmount + '\'' + 
			",totalOrders = '" + totalOrders + '\'' + 
			",orderDate = '" + orderDate + '\'' + 
			"}";
		}
}