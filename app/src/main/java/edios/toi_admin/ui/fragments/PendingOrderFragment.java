package edios.toi_admin.ui.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import edios.toi_admin.R;
import edios.toi_admin.model.CustomerOrdersItem;
import edios.toi_admin.model.ResultOutput;
import edios.toi_admin.ui.adapter.OrdersRecyclerAdapter;
import edios.toi_admin.utils.AppConstants;
import edios.toi_admin.utils.Utils;
import edios.toi_admin.utils.printing.BluetoothService;
import edios.toi_admin.utils.printing.DeviceListActivity;
import edios.toi_admin.utils.printing.SingletonBluetoothService;

public class PendingOrderFragment extends Fragment  {

    private OrdersRecyclerAdapter ordersRecyclerAdapter;
    View rootView;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ResultOutput resultOutput =
                    intent.hasExtra(AppConstants.UPDATE_ORDERS)
                            ? (ResultOutput) intent.getParcelableExtra(AppConstants.UPDATE_ORDERS) : new ResultOutput();
            if (ordersRecyclerAdapter != null) {
                ordersRecyclerAdapter.updateList(new Utils(getActivity()).orderList(AppConstants.PENDING_ORDER_STATUS, resultOutput.getCustomerOrders()));
                ordersRecyclerAdapter.notifyDataSetChanged();
                if(ordersRecyclerAdapter.getItemCount()<1) {
                    rootView.findViewById(R.id.tv_noOrderFound).setVisibility(View.VISIBLE);
                }else{
                    rootView.findViewById(R.id.tv_noOrderFound).setVisibility(View.GONE);
                }

            }
        }
    };


    public PendingOrderFragment() {
    }

    @Override
    public void onResume() {
        System.out.println("PendingOrderFragment.onResume");
        super.onResume();
        Objects.requireNonNull(getActivity()).registerReceiver(receiver, new IntentFilter(AppConstants.UPDATE_ORDERS));

    }

    @Override
    public void onPause() {
        System.out.println("PendingOrderFragment.onPause");
        super.onPause();
        Objects.requireNonNull(getActivity()).unregisterReceiver(receiver);


    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.out.println("PendingOrderFragment.onCreateView");
       rootView = inflater.inflate(R.layout.fragment_pending_order, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.ordersRecycler);
        ordersRecyclerAdapter = new OrdersRecyclerAdapter(getActivity(), new ArrayList<CustomerOrdersItem>(), true);
        recyclerView.setAdapter(ordersRecyclerAdapter);
        return rootView;
    }


}
