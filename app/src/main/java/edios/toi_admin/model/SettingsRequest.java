package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;

public class SettingsRequest {

    @SerializedName("signatureKey")
    private String signatureKey;

    @SerializedName("userName")
    private String userName;
    @SerializedName("readyTimeInMin")
    private String readyTimeInMin;
    @SerializedName("accountID")
    private Long accountID;
    @SerializedName("siteID")
    private Long siteID;

    public void setAccountID(Long accountID) {
        this.accountID = accountID;
    }

    public void setSiteID(Long siteID) {
        this.siteID = siteID;
    }


    public void setSignatureKey(String signatureKey) {
        this.signatureKey = signatureKey;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setReadyTimeInMin(String readyTimeInMin) {
        this.readyTimeInMin = readyTimeInMin;
    }
}
