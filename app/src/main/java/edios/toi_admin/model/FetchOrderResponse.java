package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;

public class FetchOrderResponse{

	@SerializedName("Result_Status")
	private String resultStatus;

	@SerializedName("Result_Message")
	private String resultMessage;

	@SerializedName("Updated_Date")
	private String updatedDate;

	@SerializedName("Result_Output")
	private ResultOutput resultOutput;

	@SerializedName("Result_Code")
	private String resultCode;

	@SerializedName("Site_Details")
	private SiteDetails siteDetails;

	public SiteDetails getSiteDetails() {
		return siteDetails;
	}

	public String getResultStatus(){
		return resultStatus;
	}

	public String getResultMessage(){
		return resultMessage;
	}

	public String getUpdatedDate(){
		return updatedDate;
	}

	public ResultOutput getResultOutput(){
		return resultOutput;
	}

	public String getResultCode(){
		return resultCode;
	}

	@Override
 	public String toString(){
		return 
			"FetchOrderResponse{" + 
			"result_Status = '" + resultStatus + '\'' + 
			",result_Message = '" + resultMessage + '\'' + 
			",updated_Date = '" + updatedDate + '\'' + 
			",result_Output = '" + resultOutput + '\'' + 
			",result_Code = '" + resultCode + '\'' + 
			"}";
		}
}