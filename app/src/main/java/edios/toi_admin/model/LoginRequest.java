package edios.toi_admin.model;


import com.google.gson.annotations.SerializedName;


public class LoginRequest{
	@SerializedName("userPassword")
	private String userPassword;
	@SerializedName("signatureKey")
	private String signatureKey;
	@SerializedName("userName")
	private String userName;
	@SerializedName("accountID")
	private Long accountID;
	@SerializedName("siteID")
	private Long siteID;

	public void setAccountID(Long accountID) {
		this.accountID = accountID;
	}

	public void setSiteID(Long siteID) {
		this.siteID = siteID;
	}

	public void setUserPassword(String userPassword){
		this.userPassword = userPassword;
	}

	public String getUserPassword(){
		return userPassword;
	}

	public void setSignatureKey(String signatureKey){
		this.signatureKey = signatureKey;
	}

	public String getSignatureKey(){
		return signatureKey;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	@Override
 	public String toString(){
		return 
			"LoginRequest{" + 
			"userPassword = '" + userPassword + '\'' + 
			",signatureKey = '" + signatureKey + '\'' + 
			",userName = '" + userName + '\'' + 
			"}";
		}
}