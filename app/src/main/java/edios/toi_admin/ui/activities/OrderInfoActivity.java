package edios.toi_admin.ui.activities;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import edios.toi_admin.R;
import edios.toi_admin.model.CustomerOrdersItem;
import edios.toi_admin.ui.adapter.OrderItemsRecyclerAdapter;
import edios.toi_admin.utils.AppConstants;
import edios.toi_admin.utils.Utils;

public class OrderInfoActivity extends AppCompatActivity {
    @BindView(R.id.profile_image)
    CircleImageView civ_customerProfilePic;
    @BindView(R.id.tv_orderInstructions)
    TextView tv_orderInstructions;
    @BindView(R.id.tv_orderNumber)
    TextView tv_orderNumber;
    @BindView(R.id.tv_orderStatus)
    TextView tv_orderStatus;
    @BindView(R.id.tv_customerName)
    TextView tv_customerName;
    @BindView(R.id.tv_customerMobile)
    TextView tv_customerMobile;
    @BindView(R.id.tv_orderTotalAmount)
    TextView tv_orderTotalAmount;
    @BindView(R.id.tv_totalDiscount)
    TextView tv_totalDiscount;
    @BindView(R.id.tv_totalTax)
    TextView tv_totalTax;
    @BindView(R.id.rv_items)
    RecyclerView rv_itemRecyclerView;
    @BindString(R.string.order_info_title)
    String title;
    CustomerOrdersItem order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_info);
        ButterKnife.bind(this);
        order = getIntent().getParcelableExtra("orderInfo");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
        setTitle(title);
        init();
        System.out.println("order = " + order.toString());


    }

    private void init() {
        Glide.with(this).load(order.getCustomer().getUserImageURL()).into(civ_customerProfilePic);
        tv_orderNumber.setText(String.format("%s (%s)", order.getOrderNumber(), order.getOrderStatus()));
        tv_customerName.setText(order.getCustomer().getUserName());
        tv_customerMobile.setText(order.getCustomer().getMobileNo());
        tv_orderStatus.setText(order.getOrderDateTime());
        rv_itemRecyclerView.setAdapter(new OrderItemsRecyclerAdapter(this, order.getPreviousOrderItems(), true));
        if(order.getTotalDiscountAmount()!=0){
            tv_totalDiscount.setVisibility(View.VISIBLE);
            tv_totalDiscount.setText("Total Discount: ".concat(AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalDiscountAmount())));
        }
        if(order.getTotalTaxAmount()!=0){
            tv_totalTax.setVisibility(View.VISIBLE);
            tv_totalTax.setText("Total Tax: ".concat(AppConstants.CURRENCY) .concat(Utils.decimalFormat.format(order.getTotalTaxAmount())));
        }
        tv_orderTotalAmount.setText(String.format("Total Amount: $%s", Utils.decimalFormat.format(order.getTotalOrderAmount())));
        tv_orderInstructions.setText(TextUtils.isEmpty(order.getOrderInstructions()) ? "N/A" : order.getOrderInstructions());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
