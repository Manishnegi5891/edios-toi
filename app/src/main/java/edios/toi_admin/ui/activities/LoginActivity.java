package edios.toi_admin.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import edios.toi_admin.R;
import edios.toi_admin.interfaces.UpdateAppDialogListener;
import edios.toi_admin.model.GenericResponse;
import edios.toi_admin.model.LoginRequest;
import edios.toi_admin.model.SiteDetails;
import edios.toi_admin.network.NetworkSingleton;
import edios.toi_admin.utils.AppConstants;
import edios.toi_admin.utils.SharedPref;
import edios.toi_admin.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements UpdateAppDialogListener {

    @BindView(R.id.user_name)
    EditText et_userName;
    @BindView(R.id.password)
    EditText et_password;
    @BindView(R.id.tv_version)
    TextView tv_version;
    @BindString(R.string.app_name)
    String app_name;
    Utils utils;
    SharedPreferences prefReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if (SharedPref.getReader(this).getBoolean(AppConstants.LOGIN_STATUS, false)) {
            startActivity(new Intent(this, DrawerActivity.class));
            finish();
        }
        utils = new Utils(this);
        prefReader = SharedPref.getReader(LoginActivity.this);
        tv_version.setText("Version - " + utils.getAppVersion());
    }

    @Optional
    @OnClick(R.id.login)
    public void performLogin(View view) {
        if (utils.isNetworkConnected()) {
            final MaterialDialog progressDialog = utils.showProgressDialog(AppConstants.AUTHENTICATE_MESSAGE);
            NetworkSingleton.getInstance(this).authenticate(preapareLoginRequest()).enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    progressDialog.dismiss();
                    if (response.body() != null) {
                        Toast.makeText(LoginActivity.this, response.body().getResultMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().getSiteDetails() != null) {
                            storeLoginInfo(response.body().getSiteDetails());
                            if (utils.validateAppVersion(response.body().getSiteDetails())) {
                                if (response.body().getResultCode().startsWith("S")) {
                                    launchDashBoard();
                                }
                            } else {
                                utils.updateAppDialog(response.body().getSiteDetails(), LoginActivity.this);

                            }
                        }
                    } else
                        Toast.makeText(LoginActivity.this, AppConstants.SERVER_NOT_RESPONDING_MESSAGE, Toast.LENGTH_SHORT).show();


                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    System.out.println("LoginActivity.onFailure " + t.getMessage());
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, AppConstants.SERVER_NOT_CONNECTED, Toast.LENGTH_SHORT).show();

                }

            });
        } else {
            Toast.makeText(this, AppConstants.INTERNET_CONNECTION_MESSAGE, Toast.LENGTH_SHORT).show();
        }

    }

    private void launchDashBoard() {
        Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
        startActivity(intent);
        finish();
    }


    private void storeLoginInfo(SiteDetails siteDetails) {
        SharedPreferences.Editor prefWriter = SharedPref.getEditor(LoginActivity.this);
        prefWriter.putBoolean(AppConstants.LOGIN_STATUS, true).putString(AppConstants.USER_NAME, et_userName.getText().toString());
        if (siteDetails != null) {
            prefWriter.putString(AppConstants.ORDER_READY_TIME, String.valueOf(siteDetails.getOrderReadyTime())).putString(AppConstants.ORDER_NO_PREFIX, siteDetails.getOrderPrefix()).putString(AppConstants.SITE_CURRENCY, siteDetails.getSiteCurrency()).putString(AppConstants.SITE_TIME_ZONE, siteDetails.getTimeZone());
        }

        prefWriter.apply();

    }

    private LoginRequest preapareLoginRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUserName(et_userName.getText().toString());
        loginRequest.setAccountID(AppConstants.ACCOUNT_ID);
        loginRequest.setSiteID(AppConstants.SITE_ID);
        loginRequest.setUserPassword(et_password.getText().toString());
        loginRequest.setSignatureKey(AppConstants.SIGNATURE_KEY);

        return loginRequest;
    }

    @Override
    public void onPositiveClick(DialogInterface dialog, int which) {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void onNegativeClick(DialogInterface dialog, int which) {
        launchDashBoard();
        utils.saveAppVerLastCheckedAt();
    }
}
