package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;

public class SearchOrderRequest {
    private Long accountID,siteID;
    @SerializedName("signatureKey")
    private String signatureKey;
    @SerializedName("userName")
    private String userName;
    @SerializedName("customerName")
    private String customerName;
    @SerializedName("mobileNo")
    private String mobileNo;
    @SerializedName("eMailAddress")
    private String eMailAddress;
    @SerializedName("orderNo")
    private String orderNo;
	public void setAccountID(Long accountID) {
		this.accountID = accountID;
	}

	public void setSiteID(Long siteID) {
		this.siteID = siteID;
	}
    public void setSignatureKey(String signatureKey) {
        this.signatureKey = signatureKey;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public void seteMailAddress(String eMailAddress) {
        this.eMailAddress = eMailAddress;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
