package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;


public class OrderSummaryRequest {
    private Long accountID, siteID;
    @SerializedName("fromDate")
    private String fromDate;

    @SerializedName("signatureKey")
    private String signatureKey;

    @SerializedName("toDate")
    private String toDate;

    @SerializedName("userName")
    private String userName;

    public void setAccountID(Long accountID) {
        this.accountID = accountID;
    }

    public void setSiteID(Long siteID) {
        this.siteID = siteID;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public void setSignatureKey(String signatureKey) {
        this.signatureKey = signatureKey;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "OrderSummaryRequest{" + "fromDate = '" + fromDate + '\'' + ",signatureKey = '" + signatureKey + '\'' + ",toDate = '" + toDate + '\'' + ",userName = '" + userName + '\'' + "}";
    }
}