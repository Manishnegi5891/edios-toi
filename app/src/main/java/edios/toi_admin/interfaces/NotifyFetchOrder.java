package edios.toi_admin.interfaces;

import edios.toi_admin.model.ResultOutput;

public interface NotifyFetchOrder {
    void orderFetched(ResultOutput orders, boolean isSuccess);
}
