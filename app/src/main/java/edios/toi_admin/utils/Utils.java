package edios.toi_admin.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import edios.toi_admin.R;
import edios.toi_admin.interfaces.NotifyFetchOrder;
import edios.toi_admin.interfaces.UpdateAppDialogListener;
import edios.toi_admin.model.CustomerOrdersItem;
import edios.toi_admin.model.FetchOrderRequest;
import edios.toi_admin.model.FetchOrderResponse;
import edios.toi_admin.model.PreviousOrderItems;
import edios.toi_admin.model.ResultOutput;
import edios.toi_admin.model.SiteDetails;
import edios.toi_admin.network.NetworkSingleton;
import edios.toi_admin.ui.fragments.ChangePasswordFragment;
import edios.toi_admin.ui.fragments.OrderHistoryFragment;
import edios.toi_admin.ui.fragments.OrderSummaryFragment;
import edios.toi_admin.ui.fragments.OrdersFragment;
import edios.toi_admin.ui.fragments.SearchOrdersFragment;
import edios.toi_admin.ui.fragments.SettingsFragment;
import edios.toi_admin.utils.printing.BluetoothService;
import edios.toi_admin.utils.printing.PrinterCommands;
import edios.toi_admin.utils.printing.SingletonBluetoothService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Utils {
    public static DecimalFormat decimalFormat = new DecimalFormat("0.00");
    AppCompatActivity activity;
    Context context;
    private BluetoothService mService;
    private SharedPreferences prefReader;
    private SimpleDateFormat apiDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    public Utils(Object context) {
        if (context instanceof AppCompatActivity) {
            this.activity = (AppCompatActivity) context;
            prefReader = SharedPref.getReader(activity);
//        this.context = ((AppCompatActivity) context).getBaseContext();
        } else if (context instanceof Context) {
            this.context = (Context) context;
            System.out.println("instance of Context");
        }
    }


    public MaterialDialog showProgressDialog(String msg) {
        try {
            return new MaterialDialog.Builder(activity).
                    title(activity.getResources().getString(R.string.app_name)).cancelable(false).content(msg).progress(true, 0).show();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    public void updateStatusDialog(int selectedIndex, MaterialDialog.ListCallbackSingleChoice callback) {
        new MaterialDialog.Builder(activity).
                title(activity.getResources().getString(R.string.update_status)).items(R.array.status_array).
                cancelable(false).
                itemsCallbackSingleChoice(selectedIndex, callback).positiveText(R.string.submit).negativeText(R.string.cancel).show();
    }

    public List<CustomerOrdersItem> orderList(String orderStatus, List<CustomerOrdersItem> customerOrders) {
        List<CustomerOrdersItem> inKitchenOrders = new ArrayList<>();
        for (CustomerOrdersItem order : customerOrders) {
            if (order.getOrderStatus().equalsIgnoreCase(orderStatus)) inKitchenOrders.add(order);
        }
        return inKitchenOrders;
    }

    public void fetchOrders(final NotifyFetchOrder notifyFetchOrder, final String date) {
        try {
            if (isNetworkConnected()) {
                final MaterialDialog progressDialog = showProgressDialog(AppConstants.FETCHING_ORDERS_MESSAGE);
                NetworkSingleton.getInstance(activity).fetchOrders(prepareFetchOrderRequest(date)).enqueue(new Callback<FetchOrderResponse>() {
                    @Override
                    public void onResponse(Call<FetchOrderResponse> call, @NonNull Response<FetchOrderResponse> response) {
                        System.out.println("Utils.onResponse");

                        System.out.println(response.toString());
                        if (progressDialog != null) progressDialog.dismiss();
                        if(response.body()!=null) {
                            if (validateAppVersion(response.body().getSiteDetails())) {
                                if (date.length() == 0 && SharedPref.getReader(activity).getBoolean(AppConstants.AUTO_PRINT, false)) {
                                    autoPrintBill(response.body().getResultOutput());
                                }
                                notifyFetchOrder.orderFetched(response.body().getResultOutput(), true);

                                int totalReceivedOrders = response.body().getResultOutput().getCustomerOrders().size();
                                if (totalReceivedOrders > 0) {
                                    SharedPref.getEditor(activity).
                                            putInt(AppConstants.LAST_RECEIVED_ORDER, response.body().getResultOutput().getCustomerOrders().get(totalReceivedOrders - 1).getCustomerOrderID()).apply();
                                }
                            } else {
                                updateAppDialog(response.body().getSiteDetails(), (UpdateAppDialogListener) activity);
                            }
                        }
                        else{
                            if(activity!=null)
                            Toast.makeText(activity, AppConstants.SERVER_NOT_RESPONDING_MESSAGE, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<FetchOrderResponse> call, Throwable t) {
                        System.out.println("OrdersFragment.onFailure " + t.getMessage());
                        if(activity!=null)
                            Toast.makeText(activity, AppConstants.SERVER_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
                        notifyFetchOrder.orderFetched(new ResultOutput(), false);
                        if (progressDialog != null) progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(activity, AppConstants.INTERNET_CONNECTION_MESSAGE, Toast.LENGTH_SHORT).show();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void autoPrintBill(ResultOutput resultOutput) {
        int lastReceivedOrder = SharedPref.getReader(activity).getInt(AppConstants.LAST_RECEIVED_ORDER, 0);
        System.out.println("lastReceivedOrder = " + lastReceivedOrder);
        String[] printSize = new String[3];
        if (activity != null)
            printSize = activity.getResources().getStringArray(R.array.print_size_array);
        if (context != null)
            printSize = activity.getResources().getStringArray(R.array.print_size_array);

        boolean isNewOrder = false;
        for (CustomerOrdersItem order : resultOutput.getCustomerOrders())
            if (lastReceivedOrder < order.getCustomerOrderID()) {
                System.out.println("NEW order -- " + order.getOrderNumber());
                String selectedPrintSize = SharedPref.getReader(activity).getString(AppConstants.PRINT_SIZE, printSize[0]);
                if (selectedPrintSize.equalsIgnoreCase(printSize[0]))
                    print3InchKitchenTicket(order);
                else if (selectedPrintSize.equalsIgnoreCase(printSize[1]))
                    print3InchKitchenTicket(order);
                else print2InchKitchenTicket(order);

                isNewOrder = true;
            }
        if (!isNewOrder) {
            System.out.println("No new order found.");
        }


    }

    private FetchOrderRequest prepareFetchOrderRequest(String date) {
        FetchOrderRequest request = new FetchOrderRequest();
        request.setSignatureKey(AppConstants.SIGNATURE_KEY);
        request.setAccountID(AppConstants.ACCOUNT_ID);
        request.setSiteID(AppConstants.SITE_ID);
        request.setUserName(SharedPref.getReader(activity).getString(AppConstants.USER_NAME, ""));
        if (date.length() > 0) {
            request.setLastUpdatedDate(date);
        } else {
            Calendar cal = Calendar.getInstance();
            request.setLastUpdatedDate(apiDateFormat.format(cal.getTime()));
        }
        return request;
    }

    public void launchFragment(int menuID) {
        Fragment fragment = null;
        switch (menuID) {
            case R.id.nav_order:
                activity.setTitle(R.string.dashboard_menu);
                fragment = new OrdersFragment();
                break;
            case R.id.nav_order_history:
                activity.setTitle(R.string.order_history_menu);
                fragment = new OrderHistoryFragment();
                break;
            case R.id.nav_order_summary:
                activity.setTitle(R.string.order_summary_menu);
                fragment = new OrderSummaryFragment();
                break;

            case R.id.nav_search_order:
                activity.setTitle(R.string.search_orders_menu);
                fragment = new SearchOrdersFragment();
                break;
            case R.id.nav_change_password:
                activity.setTitle(R.string.change_password_menu);
                fragment = new ChangePasswordFragment();
                break;
            case R.id.nav_setting:
                activity.setTitle(R.string.settings_menu);
                fragment = new SettingsFragment();
                break;
        }
        if (fragment != null) {
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        } else {
            System.out.println("Fragment is null ");
        }


        DrawerLayout drawer = activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        System.out.println("DrawerActivity.launchFragment " + menuID);

    }

    public String appDateToApiDate(String apiDate) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);
        String apiFormatDate = "";
        try {

            Date date = sdf1.parse(apiDate);

            apiFormatDate = sdf2.format(date);
            System.out.println("apiFormatDate = " + apiFormatDate);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return apiFormatDate;
    }

    public String getFirstDateOfMonth() {
        String date;
        Calendar c = Calendar.getInstance();   // this takes current date
        c.set(Calendar.DAY_OF_MONTH, 1);

        date = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH).format(c.getTime());
        System.out.println("Date " + date);
        return date;
    }

    public String getCurrentDate() {
        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, -1);
        Date d = cal.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);
        return sdf1.format(d);

    }

    public void print2InchBill(CustomerOrdersItem order) {
        System.out.println("Utils.print2InchBill");
        mService = SingletonBluetoothService.getInstance(activity, null);
        String itemFormat = "%s %10s"; //39
        printCustom(AppConstants.RESTAURANT_NAME, 2, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_WEBSITE, 0, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_CONTACT, 0, 1);
        printNewLine();

        printCustom("ORDER#:  " + order.getOrderNumber(), 0, 0);
        printNewLine();
        printCustom(order.getOrderDateTime(), 0, 0);
        printNewLine();

        printCustom("================================", 1, 0);


        for (PreviousOrderItems item : order.getPreviousOrderItems()) {
            String itemName = item.getItemQuantity() + " " + item.getItemName();
            if (itemName.length() >= 20)
                itemName = itemName.substring(0, Math.min(itemName.length(), 20));
            else {
                StringBuilder sd = new StringBuilder();
                for (int i = 0; i < 20 - itemName.length(); i++) {
                    sd.append(" ");
                }
                itemName += sd;
            }
            printCustom(String.format(itemFormat, itemName, "$" + Utils.decimalFormat.format(item.getItemPrice())), 0, 0);
            printNewLine();
        }
        printCustom("================================", 1, 0);

        System.out.printf("%.2f %n", order.getTotalOrderAmount());

        printCustom(String.format(itemFormat, "Subtotal      ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalPrice())), 0, 0);
        printNewLine();
        printCustom(String.format(itemFormat, "Tax           ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalTaxAmount())), 0, 0);
        printNewLine();
        printCustom(String.format(itemFormat, "GRAND TOTAL   ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalOrderAmount())), 1, 0);
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();

    }

    public void print3InchBill(CustomerOrdersItem order) {
        System.out.println("Utils.print3InchBill");
        mService = SingletonBluetoothService.getInstance(activity, null);
        String itemFormat = "%s %15s"; //39
        printCustom(AppConstants.RESTAURANT_NAME, 2, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_WEBSITE, 0, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_CONTACT, 0, 1);
        printNewLine();

        printCustom("ORDER#:  " + order.getOrderNumber(), 0, 0);
        printNewLine();
        printCustom(order.getOrderDateTime(), 0, 0);
        printNewLine();

        printCustom("================================================", 1, 0);
        printNewLine();


        for (PreviousOrderItems item : order.getPreviousOrderItems()) {
            String itemName = item.getItemQuantity() + " " + item.getItemName();
            if (itemName.length() >= 25)
                itemName = itemName.substring(0, Math.min(itemName.length(), 25));
            else {
                StringBuilder sd = new StringBuilder();
                for (int i = 0; i < 25 - itemName.length(); i++) {
                    sd.append(" ");
                }
                itemName += sd;
            }
            printCustom(String.format(itemFormat, itemName, "$" + Utils.decimalFormat.format(item.getItemPrice())), 0, 0);
            printNewLine();
        }
        printCustom("================================================", 1, 0);
        printNewLine();
        System.out.printf("%.2f %n", order.getTotalOrderAmount());

        printCustom(String.format(itemFormat, "Subtotal      ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalPrice())), 0, 0);
        printNewLine();
        printCustom(String.format(itemFormat, "Tax           ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalTaxAmount())), 0, 0);
        printNewLine();
        printCustom(String.format(itemFormat, "GRAND TOTAL   ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalOrderAmount())), 1, 0);
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();

    }

    public void print3InchTSPBill(CustomerOrdersItem order) {
        System.out.println("Utils.print3InchBill");
        mService = SingletonBluetoothService.getInstance(activity, null);
        String itemFormat = "%s %15s"; //39
        printCustom(AppConstants.RESTAURANT_NAME, 2, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_WEBSITE, 0, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_CONTACT, 0, 1);
        printNewLine();

        printCustom("ORDER#:  " + order.getOrderNumber(), 0, 0);
        printNewLine();
        printCustom(order.getOrderDateTime(), 0, 0);
        printNewLine();

        printCustom("==========================================", 1, 0);
        printNewLine();


        for (PreviousOrderItems item : order.getPreviousOrderItems()) {
            String itemName = item.getItemQuantity() + " " + item.getItemName();
            if (itemName.length() >= 25)
                itemName = itemName.substring(0, Math.min(itemName.length(), 25));
            else {
                StringBuilder sd = new StringBuilder();
                for (int i = 0; i < 25 - itemName.length(); i++) {
                    sd.append(" ");
                }
                itemName += sd;
            }
            printCustom(String.format(itemFormat, itemName, "$" + Utils.decimalFormat.format(item.getItemPrice())), 0, 0);
            printNewLine();
        }
        printCustom("==========================================", 1, 0);
        printNewLine();
        System.out.printf("%.2f %n", order.getTotalOrderAmount());

        printCustom(String.format(itemFormat, "Subtotal      ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalPrice())), 0, 0);
        printNewLine();
        printCustom(String.format(itemFormat, "Tax           ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalTaxAmount())), 0, 0);
        printNewLine();
        printCustom(String.format(itemFormat, "GRAND TOTAL   ", AppConstants.CURRENCY).concat(Utils.decimalFormat.format(order.getTotalOrderAmount())), 1, 0);
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        cutPaper();

    }

    public void print2InchKitchenTicket(CustomerOrdersItem order) {
        System.out.println("Utils.printBill");
        mService = SingletonBluetoothService.getInstance(activity, null);
        String itemFormat = "%s %10s"; //39
       /* printCustom(AppConstants.RESTAURANT_NAME, 0, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_CONTACT, 0, 1);
        printNewLine();*/
        printCustom("Kitchen Ticket", 0, 1);
        printNewLine();


        printCustom("ORDER#:  " + order.getOrderNumber(), 1, 1);
        printNewLine();
        printCustom(order.getOrderDateTime(), 0, 1);
        printNewLine();
        String pickupTime[] = order.getOrderPickupDateTime().split(" ");
        printCustom("Pickup Time: " + pickupTime[pickupTime.length - 2] + " " + pickupTime[pickupTime.length - 1], 1, 1);
        printNewLine();
        printCustom(order.getCustomer().getUserName() + " - " + order.getCustomer().getMobileNo(), 0, 1);
        printNewLine();


        printCustom("--------------------------------", 1, 0);
        for (PreviousOrderItems item : order.getPreviousOrderItems()) {
            String itemName = item.getItemQuantity() + " " + item.getItemName();
            printCustom(String.format(itemFormat, itemName, ""), 3, 0);
            printNewLine();


            if (!TextUtils.isEmpty(item.getItemSpicyLevel())) {
                printCustom(item.getItemSpicyLevel(), 1, 0);
                printNewLine();
            }
            if (!TextUtils.isEmpty(item.getItemInstructions())) {
                printCustom(item.getItemInstructions(), 1, 0);
                printNewLine();
            }
            printCustom("--------------------------------", 1, 0);
        }

        if (!TextUtils.isEmpty(order.getOrderInstructions())) {
            printCustom("  " + order.getOrderInstructions(), 1, 0);
            printNewLine();
        }
        printCustom("================================", 1, 0);


        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
    }

    public void print3InchKitchenTicket(CustomerOrdersItem order) {
        System.out.println("Utils.printBill");
        mService = SingletonBluetoothService.getInstance(activity, null);
        String itemFormat = "%s %15s"; //39
      /*  printCustom(AppConstants.RESTAURANT_NAME, 0, 1);
        printNewLine();
        printCustom(AppConstants.RESTAURANT_CONTACT, 0, 1);
        printNewLine();*/
        printCustom("Kitchen Ticket", 0, 1);
        printNewLine();


        printCustom("ORDER#:  " + order.getOrderNumber(), 1, 1);
        printNewLine();
        printCustom(order.getOrderDateTime(), 0, 1);
        printNewLine();
        String pickupTime[] = order.getOrderPickupDateTime().split(" ");
        printCustom("Pickup Time: " + pickupTime[pickupTime.length - 2] + " " + pickupTime[pickupTime.length - 1], 1, 1);
        printNewLine();
        printCustom(order.getCustomer().getUserName() + " - " + order.getCustomer().getMobileNo(), 0, 1);
        printNewLine();


        printCustom("------------------------------------------------", 1, 0);
        printNewLine();
        for (PreviousOrderItems item : order.getPreviousOrderItems()) {
            String itemName = item.getItemQuantity() + " " + item.getItemName();
            printCustom(String.format(itemFormat, itemName, ""), 3, 0);
            printNewLine();


            if (!TextUtils.isEmpty(item.getItemSpicyLevel())) {
                printCustom(item.getItemSpicyLevel(), 1, 0);
                printNewLine();
            }
            if (!TextUtils.isEmpty(item.getItemInstructions())) {
                printCustom(item.getItemInstructions(), 1, 0);
                printNewLine();
            }
            printCustom("------------------------------------------------", 1, 0);
            printNewLine();
        }

        if (!TextUtils.isEmpty(order.getOrderInstructions())) {
            printCustom("  " + order.getOrderInstructions(), 1, 0);
            printNewLine();
        }
        printCustom("================================================", 1, 0);
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
    }


    public void print3InchTSPKitchenTicket(CustomerOrdersItem order) {
        System.out.println("Utils.printBill");
        mService = SingletonBluetoothService.getInstance(activity, null);
        String itemFormat = "%s %15s"; //39

        printCustom("Kitchen Ticket", 0, 1);
        printNewLine();


        printCustom("ORDER#:  " + order.getOrderNumber(), 1, 1);
        printNewLine();
        printCustom(order.getOrderDateTime(), 0, 1);
        printNewLine();
        String pickupTime[] = order.getOrderPickupDateTime().split(" ");
        printCustom("Pickup Time: " + pickupTime[pickupTime.length - 2] + " " + pickupTime[pickupTime.length - 1], 1, 1);
        printNewLine();
        printCustom(order.getCustomer().getUserName() + " - " + order.getCustomer().getMobileNo(), 0, 1);
        printNewLine();


        printCustom("------------------------------------------", 1, 0);
        printNewLine();
        for (PreviousOrderItems item : order.getPreviousOrderItems()) {
            String itemName = item.getItemQuantity() + " " + item.getItemName();
            printCustom(String.format(itemFormat, itemName, ""), 3, 0);
            printNewLine();


            if (!TextUtils.isEmpty(item.getItemSpicyLevel())) {
                specifyRedPrinting();
                printCustom(item.getItemSpicyLevel(), 1, 0);
                cancelRedPrinting();
                printNewLine();
            }
            if (!TextUtils.isEmpty(item.getItemInstructions())) {
                specifyRedPrinting();
                printCustom(item.getItemInstructions(), 1, 0);
                cancelRedPrinting();
                printNewLine();
            }
            printCustom("------------------------------------------", 1, 0);
            printNewLine();
        }

        if (!TextUtils.isEmpty(order.getOrderInstructions())) {
            specifyRedPrinting();
            printCustom("  " + order.getOrderInstructions(), 1, 0);
            cancelRedPrinting();
            printNewLine();
        }
        printCustom("==========================================", 1, 0);
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        printNewLine();
        cutPaper();
    }

    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
//        byte[] cc = new byte[]{0x1B, 0x21, 0x02};  // 0- normal size text
        byte[] cc1 = new byte[]{0x1B, 0x21, 0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
//        byte[] mFormat = new byte[]{27, 33, 0};
//        byte[] arrayOfByte1 = { 27, 33, 0 };
        byte[] format = {29, 33, 35};
        try {
            switch (size) {
                case 0:
                    mService.write(cc1);
                    break;
                case 1:
                    mService.write(bb);
                    break;
                case 2:
                    mService.write(bb2);
                    break;
                case 3:
                    mService.write(bb3);
                    break;
                case 99:
                    mService.write(format);
                    break;

            }

            switch (align) {
                case 0:
                    //left align
                    mService.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    mService.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    mService.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            mService.write(msg.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void printNewLine() {
        try {
            mService.write(PrinterCommands.FEED_LINE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void cutPaper() {
        try {
            mService.write(PrinterCommands.CUT_PAPER);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void specifyRedPrinting() {
        try {
            mService.write(PrinterCommands.ENABLE_RED_COLOR_PRINT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cancelRedPrinting() {
        try {
            mService.write(PrinterCommands.DISABLE_RED_COLOR_PRINT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isNetworkConnected() {
        if (activity == null) return true;
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;

    }

    public boolean validateAppVersion(SiteDetails siteDetails) {
        try {


            Date currentDate = apiDateFormat.parse(apiDateFormat.format(new Date()));
            String lastCheckedAt = SharedPref.getReader(activity != null ? activity : context).getString(AppConstants.APP_VERSION_LAST_CHECKED_AT, "");
            long diffHours = (currentDate.getTime() - apiDateFormat.parse(lastCheckedAt).getTime()) / (60 * 60 * 1000);
            System.out.println("lastCheckedAt = " + lastCheckedAt);
            System.out.println("diffHours = " + diffHours);


            if (siteDetails.getAdminAppForceUpgrade().equalsIgnoreCase("No")) {
                if (diffHours >= 24) {
                    return siteDetails.getAdminAppVersion() <= getAppVersion();
                } else {
                    return true;
                }
            } else {
                return siteDetails.getAdminAppVersion() <= getAppVersion();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return siteDetails.getAdminAppVersion() <= getAppVersion();
        }

    }


    public void updateAppDialog(SiteDetails siteDetails, final UpdateAppDialogListener listener) {

        if (activity != null) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

            dialog.setCancelable(false);
            dialog.setTitle(activity.getResources().getString(R.string.app_name));

            dialog.setPositiveButton("Update App", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (listener != null) {
                        listener.onPositiveClick(dialog, which);
                    }

                    dialog.dismiss();

                }
            });
            if (siteDetails.getAdminAppForceUpgrade().equalsIgnoreCase("No")) {
                dialog.setMessage(AppConstants.APP_UPDATE_MESSAGE).setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) {
                            listener.onNegativeClick(dialog, which);
                        }
                    }
                });
            } else {
                dialog.setMessage(AppConstants.FORCED_APP_UPDATE_MESSAGE);
            }
            dialog.create().show();
        }

    }

    public Float getAppVersion() {
        PackageInfo pInfo = null;
        try {
            if (activity != null)
                pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            else pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Float version = Float.parseFloat(pInfo.versionName.trim());
        return version;
    }

    public void saveAppVerLastCheckedAt() {
        SharedPref.getEditor(activity != null ? activity : context).putString(AppConstants.APP_VERSION_LAST_CHECKED_AT, apiDateFormat.format(new Date())).apply();
    }

}
