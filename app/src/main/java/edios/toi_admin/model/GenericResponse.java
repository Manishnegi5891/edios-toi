package edios.toi_admin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class GenericResponse implements Parcelable {

    public static final Creator<GenericResponse> CREATOR = new Creator<GenericResponse>() {
        @Override
        public GenericResponse createFromParcel(Parcel in) {
            return new GenericResponse(in);
        }

        @Override
        public GenericResponse[] newArray(int size) {
            return new GenericResponse[size];
        }
    };
    @SerializedName("Result_Status")
    private String resultStatus;
    @SerializedName("Result_Message")
    private String resultMessage;
    @SerializedName("Updated_Date")
    private String updatedDate;
    @SerializedName("Result_Output")
    private String resultOutput;
    @SerializedName("Result_Code")
    private String resultCode;
    @SerializedName("Site_Details")
    private SiteDetails siteDetails;

    protected GenericResponse(Parcel in) {
        resultStatus = in.readString();
        resultMessage = in.readString();
        updatedDate = in.readString();
        resultOutput = in.readString();
        resultCode = in.readString();
    }

    public SiteDetails getSiteDetails() {
        return siteDetails;
    }

    public void setSiteDetails(SiteDetails siteDetails) {
        this.siteDetails = siteDetails;
    }

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Object getResultOutput() {
        return resultOutput;
    }

    public void setResultOutput(String resultOutput) {
        this.resultOutput = resultOutput;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    @Override
    public String toString() {
        return "GenericResponse{" + "result_Status = '" + resultStatus + '\'' + ",result_Message = '" + resultMessage + '\'' + ",updated_Date = '" + updatedDate + '\'' + ",result_Output = '" + resultOutput + '\'' + ",result_Code = '" + resultCode + '\'' + "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(resultStatus);
        parcel.writeString(resultMessage);
        parcel.writeString(updatedDate);
        parcel.writeString(resultOutput);
        parcel.writeString(resultCode);
    }
}