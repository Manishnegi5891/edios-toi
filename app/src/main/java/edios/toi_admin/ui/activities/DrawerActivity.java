package edios.toi_admin.ui.activities;

import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import edios.toi_admin.R;
import edios.toi_admin.interfaces.BluetoothConnectivity;
import edios.toi_admin.interfaces.UpdateAppDialogListener;
import edios.toi_admin.ui.fragments.OrdersFragment;
import edios.toi_admin.utils.AlertDialogSingleton;
import edios.toi_admin.utils.AppConstants;
import edios.toi_admin.utils.SharedPref;
import edios.toi_admin.utils.Utils;
import edios.toi_admin.utils.printing.BluetoothService;
import edios.toi_admin.utils.printing.DeviceListActivity;
import edios.toi_admin.utils.printing.SingletonBluetoothService;

public class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BluetoothConnectivity, UpdateAppDialogListener {
    @BindString(R.string.log_out_message)
    String logOutMsg;
    @BindColor(R.color.colorPrimary)
    int colorAccent;
    @BindView(R.id.tv_loggedIn)
    TextView tv_loggedIn;
    Utils utils;
    NavigationView navigationView;
    private boolean doubleBackToExitPressedOnce;
    private BluetoothService mService;
    private BluetoothDevice con_dev;
    private MaterialDialog btDialog;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        utils = new Utils(this);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        utils.launchFragment(R.id.nav_order);
        mService = SingletonBluetoothService.getInstance(this, this);
        tv_loggedIn.setText("Logged In As: " + SharedPref.getReader(this).getString(AppConstants.USER_NAME, ""));
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            System.out.println(f.getTargetFragment() + "" + (f instanceof OrdersFragment));
            if (f instanceof OrdersFragment) {
                exitApp();
            } else {
                navigationView.setCheckedItem(R.id.nav_order);
                utils.launchFragment(R.id.nav_order);
            }
        }


    }

    private void exitApp() {
        if (doubleBackToExitPressedOnce) this.finish();

        doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        View parentLayout = getWindow().getDecorView().findViewById(android.R.id.content);
        final Snackbar snackbar = Snackbar.make(parentLayout, "Please click BACK again to exit", Snackbar.LENGTH_SHORT);
        snackbar.setAction("DISMISS", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
                doubleBackToExitPressedOnce = false;
            }
        }).setActionTextColor(colorAccent);

        snackbar.show();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Optional
    @OnClick(R.id.btn_log_out)
    public void logoutClick(View view) {
        AlertDialogSingleton.alertDialogShow(this, logOutMsg, false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(DrawerActivity.this, LoginActivity.class));
                SharedPref.getEditor(DrawerActivity.this).putBoolean(AppConstants.LOGIN_STATUS, false).apply();
                finish();
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        utils.launchFragment(item.getItemId());
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("DrawerActivity.onActivityResult " + resultCode + " requestCode " + requestCode);

        if (resultCode == -1) {
            if (requestCode == 1) {
                if (data.hasExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS)) {
                    btDialog = utils.showProgressDialog("Connecting printer... ");
                    con_dev = mService.getDevByMac(data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS));
                    mService.connect(con_dev);
                }
            } else if (requestCode == 2) { //after bluetooth is enabled
                startActivityForResult(new Intent(getApplicationContext(), DeviceListActivity.class), 1);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main_menu, menu);
        if (mService.getState() == BluetoothService.STATE_CONNECTED)
            menu.findItem(R.id.bluetooth).setIcon(R.drawable.ic_bluetooth_connect);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.bluetooth) {
            if (mService.isBTopen()) {
                startActivityForResult(new Intent(getApplicationContext(), DeviceListActivity.class), 1);
            } else
                startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 2);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void isConnected(boolean isConnect) {
        if (btDialog != null) {
            btDialog.dismiss();
            System.out.println("btDialog is not null");
        } else {
            System.out.println("btDialog is null");
        }
        menu.findItem(R.id.bluetooth).setIcon(isConnect ? R.drawable.ic_bluetooth_connect : R.drawable.ic_bluetooth_disconnect);

    }
    @Override
    public void onPositiveClick(DialogInterface dialog, int which) {
        System.out.println("DrawerActivity.onPositiveClick");
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void onNegativeClick(DialogInterface dialog, int which) {
        System.out.println("DrawerActivity.onNegativeClick");
        utils.saveAppVerLastCheckedAt();

    }
}
