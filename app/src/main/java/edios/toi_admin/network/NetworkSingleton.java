package edios.toi_admin.network;

import android.content.Context;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkSingleton {

    private static final String TAG = NetworkSingleton.class.getSimpleName();
//    private static final String SERVER_URL = "http://192.168.5.107:58080/TOI_API/"; //Local machine
//    private static final String SERVER_URL = "http://192.168.5.129:58080/RWP_API/"; //Test Server
    private static final String SERVER_URL = "http://ediosrwp.edios.global/RWP_API/"; //Production machine

       private static Retrofit retrofit;
    private static Calls retroInterface;


    public static Calls getInstance(Context activity) {
        initializeRetroFit(activity);
        return retroInterface;
    }

    private static void initializeRetroFit(Context activity) {
        if (retrofit == null) {
            OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(60, TimeUnit.SECONDS);
            httpClient.connectTimeout(60, TimeUnit.SECONDS);
            httpClient.addInterceptor(logging);httpClient.addInterceptor(logging);
            retrofit = new Retrofit.Builder().client(httpClient.build()).addConverterFactory(GsonConverterFactory.create()).baseUrl(SERVER_URL).client(okHttpClient).build();
            retroInterface = retrofit.create(Calls.class);
        }
    }


}
