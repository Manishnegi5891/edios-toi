package edios.toi_admin.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import java.util.Arrays;
import java.util.List;
import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import edios.toi_admin.R;
import edios.toi_admin.model.GenericResponse;
import edios.toi_admin.model.SettingsRequest;
import edios.toi_admin.network.NetworkSingleton;
import edios.toi_admin.utils.AppConstants;
import edios.toi_admin.utils.SharedPref;
import edios.toi_admin.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends Fragment {

    @BindView(R.id.switch_auto_print)
    SwitchCompat switch_autoPrint;
    @BindView(R.id.et_refresh_time)
    EditText et_refreshTime;
    @BindView(R.id.et_ready_time)
    EditText et_readyTime;

    @BindView(R.id.print_size_spinner)
    AppCompatSpinner spin_printSize;
    @BindString(R.string.print_2_inch)
    String print_2_inch;
    @BindString(R.string.print_3_inch)
    String print_3_inch;
    @BindString(R.string.print_tsp_3_inch)
    String print_tsp_3_inch;
    @BindArray(R.array.print_size_array)
    String[] print_size_array;

    SharedPreferences prefReader;
    private Context context;
    private Utils utils;
    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        initSettings();
        return view;
    }

    private void initSettings() {
        context = getActivity();
        utils = new Utils(context);
        prefReader = SharedPref.getReader(context);
        et_readyTime.setText(prefReader.getString(AppConstants.ORDER_READY_TIME, AppConstants.DEFAULT_ORDER_READY_TIME));
        switch_autoPrint.setChecked(prefReader.getBoolean(AppConstants.AUTO_PRINT, false));
        et_refreshTime.setText(Integer.toString(prefReader.getInt(AppConstants.ORDER_REFRESH_TIME, AppConstants.DEFAULT_REFRESH_TIME)));
        String selectedPrintSize=prefReader.getString(AppConstants.PRINT_SIZE, print_tsp_3_inch);
        List<String> printSize=  Arrays.asList(print_size_array);
        spin_printSize.setSelection(printSize.indexOf(selectedPrintSize));
    }

    @OnClick(R.id.btn_update_settings)
    public void onClick(View view) {
        if (validateSettings())
            if (!prefReader.getString(AppConstants.ORDER_READY_TIME, String.valueOf(AppConstants.DEFAULT_REFRESH_TIME)).equalsIgnoreCase(et_readyTime.getText().toString())) {
                if (utils.isNetworkConnected()) {
                    final MaterialDialog dialog = utils.showProgressDialog(AppConstants.UPDATE_SETTING_MESSAGE);
                    NetworkSingleton.getInstance(context).updateSettings(prepareSettingsRequest()).enqueue(new Callback<GenericResponse>() {
                        @Override
                        public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                            dialog.dismiss();
                            if(response.body()!=null) {
                                if (response.body().getResultCode().startsWith("S")) {
                                    System.out.println(response.body().toString());
                                    saveSettings();
                                }
                                Toast.makeText(context, response.body().getResultMessage(), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(context, AppConstants.SERVER_NOT_RESPONDING_MESSAGE, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GenericResponse> call, Throwable t) {
                            dialog.dismiss();
                            Toast.makeText(context, AppConstants.SERVER_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(context, AppConstants.INTERNET_CONNECTION_MESSAGE, Toast.LENGTH_SHORT).show();
                }
            } else {
                saveSettings();
                Toast.makeText(context, AppConstants.SETTINGS_SAVED_MESSAGE, Toast.LENGTH_SHORT).show();
            }

    }

    private boolean validateSettings() {

        if (et_readyTime.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Order Ready Time (In Minutes) can't be blank.", Toast.LENGTH_SHORT).show();
            return false;
        } else if (et_refreshTime.getText().toString().trim().isEmpty()) {
            Toast.makeText(context, "Dashboard Refresh Time (In Seconds) can't be blank.", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            int readyTime = Integer.parseInt(et_readyTime.getText().toString());
            int refreshTime = Integer.parseInt(et_refreshTime.getText().toString());
            if (readyTime < 5 || readyTime > 60) {
                Toast.makeText(context, "Order Ready Time should be between 5 to 60 Minutes.", Toast.LENGTH_SHORT).show();
                return false;
            } else if (refreshTime < 15 || refreshTime > 60) {
                Toast.makeText(context, "Dashboard Refresh Time should be between 15 to 60 Seconds.", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                return true;
            }
        }
    }

    private void saveSettings() {
        SharedPref.getEditor(context).putString(AppConstants.PRINT_SIZE, spin_printSize.getSelectedItem().toString()).putString(AppConstants.ORDER_READY_TIME, et_readyTime.getText().toString()).putInt(AppConstants.ORDER_REFRESH_TIME, Integer.parseInt(et_refreshTime.getText().toString())).putBoolean(AppConstants.AUTO_PRINT, switch_autoPrint.isChecked()).apply();
        utils.launchFragment(R.id.nav_order);
    }

    private SettingsRequest prepareSettingsRequest() {
        SettingsRequest request = new SettingsRequest();
        request.setUserName(SharedPref.getReader(context).getString(AppConstants.USER_NAME, ""));
        request.setSignatureKey(AppConstants.SIGNATURE_KEY);
        request.setAccountID(AppConstants.ACCOUNT_ID);
        request.setSiteID(AppConstants.SITE_ID);
        request.setReadyTimeInMin(et_readyTime.getText().toString().trim());
        return request;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
