package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderSummaryResponse{

	@SerializedName("Result_Status")
	private String resultStatus;

	@SerializedName("Result_Message")
	private String resultMessage;

	@SerializedName("Updated_Date")
	private String updatedDate;

	@SerializedName("Result_Output")
	private List<OrderSummaryItem> resultOutput;

	@SerializedName("Result_Code")
	private String resultCode;

	public String getResultStatus(){
		return resultStatus;
	}

	public String getResultMessage(){
		return resultMessage;
	}

	public String getUpdatedDate(){
		return updatedDate;
	}

	public Object getResultOutput(){
		return resultOutput;
	}

	public String getResultCode(){
		return resultCode;
	}


	@Override
	public String toString() {
		return "OrderSummaryResponse{" + "resultStatus='" + resultStatus + '\'' + ", resultMessage='" + resultMessage + '\'' + ", updatedDate='" + updatedDate + '\'' + ", resultOutput=" + resultOutput + ", resultCode='" + resultCode + '\'' + '}';
	}
}