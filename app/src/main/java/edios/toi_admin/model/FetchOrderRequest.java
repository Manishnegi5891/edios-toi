package edios.toi_admin.model;

import com.google.gson.annotations.SerializedName;

public class FetchOrderRequest{

	private Long accountID,siteID;
	@SerializedName("lastUpdatedDate")
	private String lastUpdatedDate;

	@SerializedName("signatureKey")
	private String signatureKey;

	@SerializedName("customerUserPK")
	private String customerUserPK;

	@SerializedName("userName")
	private String userName;

	@SerializedName("userEnd")
	private String userEnd;

	public void setAccountID(Long accountID) {
		this.accountID = accountID;
	}

	public void setSiteID(Long siteID) {
		this.siteID = siteID;
	}

	public String getUserEnd() {
		return userEnd;
	}

	public void setUserEnd(String userEnd) {
		this.userEnd = userEnd;
	}

	public void setLastUpdatedDate(String lastUpdatedDate){
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLastUpdatedDate(){
		return lastUpdatedDate;
	}

	public void setSignatureKey(String signatureKey){
		this.signatureKey = signatureKey;
	}

	public String getSignatureKey(){
		return signatureKey;
	}

	public void setCustomerUserPK(String customerUserPK){
		this.customerUserPK = customerUserPK;
	}

	public String getCustomerUserPK(){
		return customerUserPK;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	@Override
 	public String toString(){
		return 
			"FetchOrderRequest{" + 
			"lastUpdatedDate = '" + lastUpdatedDate + '\'' + 
			",signatureKey = '" + signatureKey + '\'' + 
			",customerUserPK = '" + customerUserPK + '\'' + 
			",userName = '" + userName + '\'' + 
			"}";
		}
}